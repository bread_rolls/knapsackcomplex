﻿namespace KnapsackContract
{
    //Класс предмета в рюкзаке
    public class KnapsackItem
    {
        //Индекс
        public int Id { get; set; }
        //Масса
        public double Mass { get; set; }
        //Стоимость
        public double Value { get; set; }
    }
}