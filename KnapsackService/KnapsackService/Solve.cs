using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Text;
using KnapsackContract;

namespace KnapsackService
{
    public static class Solve
    {
        [FunctionName("Solve")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string inputJson;
            if (req.Query.ContainsKey("input"))
                inputJson = req.Query["input"];
            else if (req.Form.ContainsKey("input"))
                inputJson = req.Form["input"];
            else
            {
                using (Stream receiveStream = req.Body)
                {
                    using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                    {
                        inputJson = readStream.ReadToEnd();
                    }
                }
                if (inputJson == null || inputJson == "")
                    return new BadRequestObjectResult("input parameter is lacking");
            }
            KnapsackInput input = JsonConvert.DeserializeObject<KnapsackInput>(inputJson);
            int n = input.Items.Count;
            int[] b = new int[n + 1];
            bool[] used = new bool[n + 1];
            bool[] result = new bool[n + 1];
            for (int j = 0; j <= n; j++)
            {
                b[j] = j;
            }
            int i = 0;
            double maxValue = 0, currentValue = 0, currentMass = 0;
            while (b[0] < n)
            {
                used[b[0]] = !used[b[0]];
                int coef = used[b[0]] ? 1 : -1;
                currentValue += coef * input.Items[b[0]].Value;
                currentMass += coef * input.Items[b[0]].Mass;
                if (currentMass <= input.MaxCapacity && currentValue > maxValue)
                {
                    maxValue = currentValue;
                    result = (bool[])used.Clone();
                }
                if (i < n)
                {
                    i = b[0];
                    b[0] = 0;
                    b[i] = b[i + 1];
                    b[i + 1] = i + 1;
                }
            }
            KnapsackSolution solution = new KnapsackSolution
            {
                Items = new List<KnapsackItem>(),
                UsedAlgorithmName = "������ �������� �� ������ ����� ����"
            };
            for (i = 0; i < n; i++)
                if (result[i])
                    solution.Items.Add(input.Items[i]);

            return new OkObjectResult(JsonConvert.SerializeObject(solution));
        }
    }
}
