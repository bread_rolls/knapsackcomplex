﻿using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace KnapsackClient
{
    public partial class MainForm : Form
    {
        KnapsackService service;
        
        public MainForm(KnapsackService service)
        {
            InitializeComponent();
            this.service = service;
        }

        private Knapsackservice.KnapsackInput parseAndValidateForm()
        {
            if (String.IsNullOrEmpty(tbN.Text.Trim())
                || String.IsNullOrEmpty(tbMaxCapacity.Text.Trim())
                || String.IsNullOrEmpty(tbMasses.Text.Trim())
                || String.IsNullOrEmpty(tbValues.Text.Trim()))
                throw new Exception(KnapsackClientErrors.EmptyFields);

            Knapsackservice.KnapsackInput input = new Knapsackservice.KnapsackInput();
            int itemsCount;
            if (!int.TryParse(tbN.Text.Trim(), out itemsCount) || itemsCount <= 0)
                throw new Exception(KnapsackClientErrors.InvalidItemsCount);
            double capacity;
            if (!double.TryParse(tbMaxCapacity.Text.Trim(), out capacity) || capacity <= 0)
                throw new Exception(KnapsackClientErrors.InvalidCapacity);
            input.MaxCapacity = capacity;
            double[] masses, values;
            try
            {
                masses = Array.ConvertAll(tbMasses.Text.Trim().Split(' '), Double.Parse);
            }
            catch(Exception)
            {
                throw new Exception(KnapsackClientErrors.InvalidMasses);
            }
            if (!masses.All((x) => x >= 0))
                throw new Exception(KnapsackClientErrors.InvalidMasses);
            try
            {
                values = Array.ConvertAll(tbValues.Text.Trim().Split(' '), Double.Parse);
            }
            catch (Exception)
            {
                throw new Exception(KnapsackClientErrors.InvalidValues);
            }
            if (!values.All((x) => x >= 0))
                throw new Exception(KnapsackClientErrors.InvalidValues);
            if (masses.Length != itemsCount)
                throw new Exception(KnapsackClientErrors.IncorrectMassesCount);
            if (values.Length != itemsCount)
                throw new Exception(KnapsackClientErrors.IncorrectValuesCount);
               
            for (int i = 0; i < itemsCount; i++)
                input.Items.Add(new Knapsackservice.Item() { Id = i + 1, Mass = masses[i], Value = values[i] });
            return input;
        }

        private void fillForm(Knapsackservice.KnapsackOutput result)
        {
            Action algAction = () => tbAlgorithm.Text = result.UsedAlgorithmName;
            tbAlgorithm.Invoke(algAction);
            Action solAction = () =>
            {
                tbSolution.Text = "";
                foreach (var item in result.Items)
                    tbSolution.Text += item.Id + " ";
            };
            tbSolution.Invoke(solAction);
        }

        private void SolveKnapsack(object input)
        {
            bool exit = false;
            while (!exit)
            {
                try
                {
                    var result = service.GetAnswer((Knapsackservice.KnapsackInput)input);
                    fillForm(result);
                    exit = true;
                }
                catch (Exception)
                {
                    DialogResult dialogResult = MessageBox.Show(KnapsackClientErrors.UnavailableService, KnapsackClientMessage.WantResend, MessageBoxButtons.YesNo);
                    if(dialogResult == DialogResult.Yes)
                    {
                        MessageBox.Show(KnapsackClientMessage.MessageResend);
                        Thread.Sleep(10000);
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        exit = true;
                    }
             }
            }
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            Knapsackservice.KnapsackInput input = null;
            try
            {
                input = parseAndValidateForm();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            tbAlgorithm.Text = "";
            tbSolution.Text = "";
            Thread t = new Thread(this.SolveKnapsack);
            t.Start(input);
        }
    }
}
