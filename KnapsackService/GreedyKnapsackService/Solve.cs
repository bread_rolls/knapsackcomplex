using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using KnapsackContract;

namespace GreedyFunction
{
    public static class Solve
    {
        [FunctionName("Solve")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");
            string inputJson;
            if (req.Query.ContainsKey("input"))
                inputJson = req.Query["input"];
            else if (req.Form.ContainsKey("input"))
                inputJson = req.Form["input"];
            else
            {
                using (Stream receiveStream = req.Body)
                {
                    using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                    {
                        inputJson = readStream.ReadToEnd();
                    }
                }
                if (inputJson == null || inputJson == "")
                    return new BadRequestObjectResult("input parameter is lacking");
            }
            KnapsackInput input = JsonConvert.DeserializeObject<KnapsackInput>(inputJson);
            var i = 0;
            double value = 0, mass = 0;
            var items = input.Items
                .Select(item => Tuple.Create(i++, item))
                .OrderByDescending(item => item.Item2.Value / item.Item2.Mass);
            var indices = new List<int>();
            foreach (var item in items)
            {
                if (mass + item.Item2.Mass > input.MaxCapacity)
                    continue;
                value += item.Item2.Value;
                mass += item.Item2.Mass;
                indices.Add(item.Item1);
            }
            indices = indices.OrderBy(index => index).ToList();
            KnapsackSolution solution = new KnapsackSolution
            {
                Items = new List<KnapsackItem>(),
                UsedAlgorithmName = "������ ��������"
            };
            for (i = 0; i < indices.Count; i++)
                solution.Items.Add(input.Items[indices[i]]);

            return new OkObjectResult(JsonConvert.SerializeObject(solution));
        }
    }
}
