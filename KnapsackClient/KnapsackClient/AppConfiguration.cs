﻿using System.Configuration;

namespace KnapsackClient
{
    public class AppConfiguration
    {
        public string KnapsackDispHost { get; set; }
        public string KnapsackDispPort { get; set; }

        public AppConfiguration()
        {
           KnapsackDispHost = ConfigurationManager.AppSettings.Get("DispHost");
           KnapsackDispPort = ConfigurationManager.AppSettings.Get("DispPort");
        }
    }
}
