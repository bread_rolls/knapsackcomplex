﻿using System;

namespace KnapsackDispatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            AppConfiguration config = new AppConfiguration();
            Dispatcher dispatcher;
            try
            {
                dispatcher = new Dispatcher(new DispatcherService(config.KnapsackServices), config);
                dispatcher.Start();
            }
            catch(Exception e)
            {
                Console.WriteLine("Unable to continue. Error: " + e.Message);
                return;
            }
            Console.WriteLine("Dispatcher started");
            while (Console.ReadLine() != "shutdown") { }
        }
    }
}
