﻿using System.Collections.Specialized;
using System.Configuration;

namespace KnapsackDispatcher
{
    public class AppConfiguration
    {
        public string KnapsackDispHost { get; set; }
        public string KnapsackDispPort { get; set; }
        public StringCollection KnapsackServices { get; set; }

        public AppConfiguration()
        {
            KnapsackDispHost = ConfigurationManager.AppSettings.Get("DispHost");
            KnapsackDispPort = ConfigurationManager.AppSettings.Get("DispPort");
            KnapsackServices = Properties.Settings.Default.FunctionServices;
        }
    }
}
