﻿using System.Collections.Generic;

namespace KnapsackContract
{
    //Вход задачи о рюкзаке
    public class KnapsackInput
    {
        //Все заданные предметы
        public List<KnapsackItem> Items { get; set; }
        //Максимальная вместимость
        public double MaxCapacity { get; set; }
    }
}