﻿using Grpc.Core;
using Knapsackservice;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace KnapsackDispatcher
{
    public class DispatcherService: KnapsackService.KnapsackServiceBase
    {
        private HttpClient client;
        private StringCollection serviceUrls;
        private SynchronizedCollection<ulong> serviceLoad;

        public DispatcherService(StringCollection serviceUrls) 
        {
            client = new HttpClient
            {
                Timeout = TimeSpan.FromMinutes(15)
            };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            this.serviceUrls = serviceUrls;
            serviceLoad = new SynchronizedCollection<ulong>();
            for (int i = 0; i < serviceUrls.Count; i++)
                serviceLoad.Add(0);
        }

        public override Task<ResponseOK> Healthcheck(GetRequest request, ServerCallContext context)
        {
            return Task.FromResult(new ResponseOK());
        }

        public override async Task<KnapsackOutput> SolveKnapsack(KnapsackInput request, ServerCallContext context)
        {
            int j = 0;
            foreach (var i in serviceLoad.ToArray().Select(load => Tuple.Create(j++, load)).OrderBy(pair => pair.Item2).Select(pair => pair.Item1))
            {
                try
                {
                    serviceLoad[i]++;
                    KnapsackOutput output = await InvokeSolver(serviceUrls[i], request);
                    serviceLoad[i]--;
                    if (output != null)
                        return output;
                }
                catch (HttpRequestException e) { Console.WriteLine(e.Message); }
            }
            throw new RpcException(new Status(StatusCode.Unavailable, "All of the supported services are out of range."));
        }

        private async Task<KnapsackOutput> InvokeSolver(string functionUrl, KnapsackInput input)
        {
            var content = new FormUrlEncodedContent(new Dictionary<string, string>() { { "input", JsonConvert.SerializeObject(input) } });
            HttpResponseMessage response = await client.PostAsync(functionUrl, content);
            if (response.StatusCode != HttpStatusCode.OK)
                return null;
            string body = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<KnapsackOutput>(body);
        }
    }
}
