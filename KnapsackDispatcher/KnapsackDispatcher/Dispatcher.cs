﻿using Grpc.Core;
using System;

namespace KnapsackDispatcher
{
    public class Dispatcher
    {
        Server server;

        public object Normservice { get; private set; }

        public Dispatcher(DispatcherService service, AppConfiguration config)
        {
            server = new Server
            {
                Services = { Knapsackservice.KnapsackService.BindService(service) },
                Ports = { new ServerPort(config.KnapsackDispHost, int.Parse(config.KnapsackDispPort), ServerCredentials.Insecure) }
            };
        }

        public void Start()
        {
            try
            {
                server.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ~Dispatcher()
        {
            if (server != null)
                server.ShutdownAsync();
        }
    }
}
