﻿using Grpc.Core;
using System;

namespace KnapsackClient
{
    public class KnapsackService
    {
        string dispHost;
        string dispPort;

        Knapsackservice.KnapsackService.KnapsackServiceClient grpcClient;

        public KnapsackService(AppConfiguration config)
        {
            dispHost = config.KnapsackDispHost;
            dispPort = config.KnapsackDispPort;
            Channel channel = new Channel(dispHost + ":" + dispPort, ChannelCredentials.Insecure);
            grpcClient = new Knapsackservice.KnapsackService.KnapsackServiceClient(channel);
        }

        public bool Healthcheck()
        {
            Knapsackservice.ResponseOK reply;
            try
            {
                reply = grpcClient.Healthcheck(new Knapsackservice.GetRequest());
            }
            catch (RpcException rpcError)
            {
                throw new Exception(rpcError.Status.Detail);
            }
            return true;
        }

        public Knapsackservice.KnapsackOutput GetAnswer(Knapsackservice.KnapsackInput input)
        {
            try
            {
                return grpcClient.SolveKnapsack(input);
            }
            catch (RpcException rpcError)
            {
                throw new Exception(rpcError.Status.Detail);
            }
        }
    }
}
