﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KnapsackClient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            AppConfiguration config = new AppConfiguration();
            KnapsackService service = new KnapsackService(config);
            try
            {
                service.Healthcheck();
            }
            catch (Exception e)
            {
                MessageBox.Show("Не удалось подключиться к серверу");
                return;
            }
            var testInput = new Knapsackservice.KnapsackInput();
            //testInput.MaxCapacity = 10.2;
            //testInput.Items.Add(new Knapsackservice.Item() { Id = 3, Mass = 10, Value = 13 });
            //var testAnswer = service.GetAnswer(testInput);
            Application.Run(new MainForm(service));
        }
    }
}
