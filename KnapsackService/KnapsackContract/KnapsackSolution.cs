﻿using System.Collections.Generic;

namespace KnapsackContract
{
    //Решение задачи о рюкзаке
    public class KnapsackSolution
    {
        //Ввыбранные для решения предметы
        public List<KnapsackItem> Items { get; set; }
        //Название использованного алгоритма
        public string UsedAlgorithmName { get; set; }
    }
}