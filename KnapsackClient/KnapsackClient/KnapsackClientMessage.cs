﻿
namespace KnapsackClient
{
    class KnapsackClientMessage
    {
        public static string WantResend = "Хотите переотправить сообщение позднее?";
        public static string MessageResend = "Сообщение будет переотправлено через 10 секунд ";
    }
}
