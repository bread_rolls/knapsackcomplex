﻿namespace KnapsackClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbN = new System.Windows.Forms.TextBox();
            this.tbMaxCapacity = new System.Windows.Forms.TextBox();
            this.tbMasses = new System.Windows.Forms.TextBox();
            this.tbValues = new System.Windows.Forms.TextBox();
            this.btnGet = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbSolution = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbAlgorithm = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Введите количество предметов:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(255, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Введите максимальную вместимость:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(320, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Введите стоимости предметов (через пробел):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(281, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Введите веса предметов (через пробел):";
            // 
            // tbN
            // 
            this.tbN.Location = new System.Drawing.Point(277, 33);
            this.tbN.Name = "tbN";
            this.tbN.Size = new System.Drawing.Size(100, 23);
            this.tbN.TabIndex = 4;
            // 
            // tbMaxCapacity
            // 
            this.tbMaxCapacity.Location = new System.Drawing.Point(277, 68);
            this.tbMaxCapacity.Name = "tbMaxCapacity";
            this.tbMaxCapacity.Size = new System.Drawing.Size(100, 23);
            this.tbMaxCapacity.TabIndex = 5;
            // 
            // tbMasses
            // 
            this.tbMasses.Location = new System.Drawing.Point(332, 110);
            this.tbMasses.Name = "tbMasses";
            this.tbMasses.Size = new System.Drawing.Size(316, 23);
            this.tbMasses.TabIndex = 6;
            // 
            // tbValues
            // 
            this.tbValues.Location = new System.Drawing.Point(332, 149);
            this.tbValues.Name = "tbValues";
            this.tbValues.Size = new System.Drawing.Size(316, 23);
            this.tbValues.TabIndex = 7;
            // 
            // btnGet
            // 
            this.btnGet.Location = new System.Drawing.Point(332, 178);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(316, 34);
            this.btnGet.TabIndex = 8;
            this.btnGet.Text = "Определить оптимальный набор предметов";
            this.btnGet.UseVisualStyleBackColor = true;
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnGet);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbValues);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbMasses);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbMaxCapacity);
            this.groupBox1.Controls.Add(this.tbN);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(661, 225);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Запрос";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.tbSolution);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.tbAlgorithm);
            this.groupBox2.Location = new System.Drawing.Point(12, 260);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(661, 113);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ответ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(286, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Используемый для вычисления алгоритм:";
            // 
            // tbSolution
            // 
            this.tbSolution.Location = new System.Drawing.Point(332, 62);
            this.tbSolution.Name = "tbSolution";
            this.tbSolution.ReadOnly = true;
            this.tbSolution.Size = new System.Drawing.Size(316, 23);
            this.tbSolution.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(320, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "Номера выбранных предметов (через пробел):";
            // 
            // tbAlgorithm
            // 
            this.tbAlgorithm.Location = new System.Drawing.Point(332, 30);
            this.tbAlgorithm.Name = "tbAlgorithm";
            this.tbAlgorithm.ReadOnly = true;
            this.tbAlgorithm.Size = new System.Drawing.Size(316, 23);
            this.tbAlgorithm.TabIndex = 4;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 385);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "Клиент рюкзачного сервиса";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbN;
        private System.Windows.Forms.TextBox tbMaxCapacity;
        private System.Windows.Forms.TextBox tbMasses;
        private System.Windows.Forms.TextBox tbValues;
        private System.Windows.Forms.Button btnGet;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbSolution;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbAlgorithm;
    }
}

