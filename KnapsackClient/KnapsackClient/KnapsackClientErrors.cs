﻿
namespace KnapsackClient
{
    class KnapsackClientErrors
    {
        public static string EmptyFields = "Заполнены не все поля";
        public static string InvalidMasses = "Массы должны быть вещественными неотрицательными числами.";
        public static string InvalidValues = "Стоимости должны быть вещественными неотрицательными числами.";
        public static string IncorrectMassesCount = "Количество весов не совпадает с количеством предметов";
        public static string IncorrectValuesCount = "Количество стоимостей не совпадает с количеством предметов";
        public static string InvalidItemsCount = "Количество предметов должно быть целым положительным числом";
        public static string InvalidCapacity = "Максимальная вместимость должна быть выщественным положительным числом";

        public static string UnavailableService = "К сожалению, сервис временно недоступен. Пожалуйста, попробуйте позже еще раз";
    }
}
